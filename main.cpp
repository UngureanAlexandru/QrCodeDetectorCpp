#include <iostream>
#include "headers/qrDetector.hpp"

int abs(int number);

int main()
{
    cv::Mat image = cv::imread("/home/alex/Projects/QrDetector/images/QrCode.png");

    if (image.empty())
    {
        printf("Error! Can't load the image! Line: %d\n", __LINE__);
        return 1;
    }

    QrDetector *qrDetector = new QrDetector();

    cv::namedWindow("Original image", cv::WINDOW_NORMAL);
    cv::namedWindow("Final image", cv::WINDOW_NORMAL);

    cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);


    cv::VideoCapture videoCapture;
    cv::Mat frame;

    if (!videoCapture.open(0))
    {
        printf("Can't open the video camera!");
        return -1;
    }

    while(1)
    {
        videoCapture >> frame;
        cv::cvtColor(frame, frame, cv::COLOR_BGR2GRAY);
        cv::Mat finalImage = qrDetector->detect(frame);

        int **coordinates = static_cast<int**>(malloc(sizeof(int*) * 1));
        uint length = qrDetector->getCoordinates(finalImage, coordinates);
        printf("Length: %d\n", length);

        int smallestX = frame.cols;
        int smallestY = frame.rows;

        for (int i = 0; i < length; i++)
        {
            frame.at<uchar>(coordinates[i][1], coordinates[i][0]) = 255;

            if (coordinates[i][0] < smallestX)
            {
                smallestX = coordinates[i][0];
            }

            if (coordinates[i][1] < smallestY)
            {
                smallestY = coordinates[i][1];
            }
            // printf("x:%d - y:%d\n", coordinates[i][0], coordinates[i][1]);
        }

        cv::putText(frame,
                    "It's working!",
                    cv::Point(smallestX, smallestY),
                    cv::FONT_HERSHEY_DUPLEX,
                    1.0,
                    CV_RGB(118, 185, 0),
                    2);

        // free(coordinates);

        cv::imshow("Original image", frame);
        cv::imshow("Final image", finalImage);

        if (cv::waitKey(10) == 27)
        {
            break;
        }
    }

    return 0;
}

int abs(int number)
{
    return number < 0 ? -number : number;
}
