#ifndef QRDETECTOR_HPP
#define QRDETECTOR_HPP

#include <opencv2/opencv.hpp>
#include <math.h>

class QrDetector
{
private:
    void checkLine(cv::Mat image, cv::Mat result, int orientation[2], int x, int y);
    int abs(int input);
    void clearImage(cv::Mat image);
    int countWhiteDots(cv::Mat image);
    int distance(int x0, int y0, int x1, int y1);
    void joinPoints(cv::Mat image);

    int iterationStepSizeSize;
    int white;
public:
    QrDetector();
    cv::Mat detect(cv::Mat image);
    uint getCoordinates(cv::Mat image, int **&result);
};

#endif // QRDETECTOR_HPP
