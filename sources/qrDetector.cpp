#include "../headers/qrDetector.hpp"

QrDetector::QrDetector()
{
    this->iterationStepSizeSize = 10;
    this->white = 140;
}

cv::Mat QrDetector::detect(cv::Mat image)
{
    cv::Mat result = cv::Mat::zeros(image.rows, image.cols, CV_8UC1);
    // result = image.clone();

    int verticalOrientation[2] = {0, 1};
    int hozizontalOrientation[2] = {1, 0};

    for (int x = 0; x < result.cols; x += iterationStepSizeSize)
    {
        for (int y = 0; y < result.rows; y += iterationStepSizeSize)
        {
            checkLine(image, result, verticalOrientation, x, y);
        }
    }

    for (int y = 0; y < result.rows; y += iterationStepSizeSize)
    {
        for (int x = 0; x < result.cols; x += iterationStepSizeSize)
        {
            checkLine(image, result, hozizontalOrientation, x, y);
        }
    }

    clearImage(result);
    joinPoints(result);

    //    if (countWhiteDots(result) > 3)
    //    {
    //        this->iterationStepSizeSize++;
    //    }

    return result;
}

void QrDetector::checkLine(cv::Mat image, cv::Mat result, int orientation[2], int x, int y)
{
    // 1-0 => horizontal | 0-1 => vertical

    if (!(orientation[0] ^ orientation[1]))
    {
        printf("[checkLine] Orientation values must be different! Line: %d\n", __LINE__);
        return;
    }

    int lineIterationStepSize = 1;

    int baseValue = orientation[0] * x + orientation[1] * y;
    int endValue = orientation[0] * image.cols + orientation[1] * image.rows;

    int firstBlack = 0;
    int firstWhite = 0;
    int blackCenter = 0;
    int secondWhite = 0;
    int secondBlack = 0;

    int diffThreshold = 3;
    int minSize = 5;

    int start = 0;
    int end = 0;

    for (int lineIndex = baseValue; lineIndex < endValue; lineIndex += lineIterationStepSize)
    {
        int pointX = orientation[0] * lineIndex + orientation[1] * x;
        int pointY = orientation[1] * lineIndex + orientation[0] * y;

        int localColor = image.at<uchar>(pointY, pointX);

        if (localColor < white && firstWhite == 0)
        {
            firstBlack++;
            start = lineIndex;
        }
        else if (localColor > white && firstBlack > 0 && blackCenter == 0)
        {
            firstWhite++;
        }
        else if(localColor < white && firstWhite > 0 && secondWhite == 0)
        {
            blackCenter++;
        }
        else if(localColor > white && blackCenter > 0 && secondBlack == 0)
        {
            secondWhite++;
        }
        else if(localColor < white && secondWhite > 0)
        {
            secondBlack++;
            end = lineIndex;
        }
        else if (localColor > white && secondBlack > 0)
        {
            break;
        }
    }

    int average = (firstBlack + firstWhite + secondBlack + secondWhite) / 4;

    bool firstBlackAccepted = abs(average - firstBlack) < diffThreshold && minSize < firstBlack;
    bool firstWihteAccepted = abs(average - firstWhite) < diffThreshold && minSize < firstWhite;
    bool blackCenterAccepted = abs(3 * average - blackCenter) < diffThreshold && minSize * 3 < blackCenter;
    bool secondWihteAccepted = abs(average - secondWhite) < diffThreshold && minSize < secondWhite;
    bool secondBlackAccepted = abs(average - secondBlack) < diffThreshold && minSize < secondBlack;

    if (
            firstBlackAccepted &&
            firstWihteAccepted &&
            blackCenterAccepted &&
            secondWihteAccepted &&
            secondBlackAccepted
            )
    {
        for (int drawIndex = start; drawIndex < end; drawIndex++)
        {
            int pointX = orientation[0] * drawIndex + orientation[1] * x;
            int pointY = orientation[1] * drawIndex + orientation[0] * y;

            result.at<uchar>(pointY, pointX) = 150;
        }
    }
}

int QrDetector::abs(int input)
{
    return input >= 0 ? input: -input;
}

void QrDetector::clearImage(cv::Mat image)
{
    for (int x = 0; x < image.cols; x++)
    {
        for (int y = 0; y < image.rows; y++)
        {
            if (
                    image.at<uchar>(y + 1, x) &&
                    image.at<uchar>(y - 1, x) &&
                    image.at<uchar>(y, x + 1) &&
                    image.at<uchar>(y, x - 1)
                    )
            {
                image.at<uchar>(y, x) = 255;
            }
        }
    }

    for (int x = 0; x < image.cols; x++)
    {
        for (int y = 0; y < image.rows; y++)
        {
            if (image.at<uchar>(y, x) < 255)
            {
                image.at<uchar>(y, x) = 0;
            }
        }
    }
}

int QrDetector::countWhiteDots(cv::Mat image)
{
    int result = 0;

    for (int x = 0; x < image.cols; x += iterationStepSizeSize)
    {
        for (int y = 0; y < image.rows; y += iterationStepSizeSize)
        {
            if (image.at<uchar>(y, x) > 200)
            {
                result++;
            }
        }
    }

    return result;
}

uint QrDetector::getCoordinates(cv::Mat image, int **&result)
{
    uint index = 0;

    int iterationStep = 10;
    bool consecutivePoint = false;

    for (int x = 0; x < image.cols; x += iterationStep)
    {
        for (int y = 0; y < image.rows; y += iterationStep)
        {
            if (image.at<uchar>(y, x) > 200 && !consecutivePoint)
            {
                consecutivePoint = true;
                result = static_cast<int**>(realloc(result, sizeof(int*) * (index + 1)));
                result[index] = static_cast<int*>(malloc(sizeof(int) * 2));
                result[index][0] = x;
                result[index][1] = y;
                index++;
            }
            else if (image.at<uchar>(y, x) < 200)
            {
                consecutivePoint = false;
            }
        }
    }
    return index;
}

int QrDetector::distance(int x0, int y0, int x1, int y1)
{
    return static_cast<int>(sqrt((x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1)));
}

void QrDetector::joinPoints(cv::Mat image)
{
    int iterationStep = 1;
    bool rightPoint = false;
    bool botPoint = false;

    for (int x = 0; x < image.cols; x += iterationStep)
    {
        for (int y = 0; y < image.rows; y += iterationStep)
        {
            rightPoint = false;
            botPoint = false;

            if (image.at<uchar>(y, x) == 255)
            {
                iterationStep = 10;

                if (image.at<uchar>(y, x + iterationStep) == 255)
                {
                    for (int index = x; index < x + iterationStep; index++)
                    {
                        image.at<uchar>(y, index) = 255;
                    }
                    rightPoint = true;
                }

                if (image.at<uchar>(y + iterationStep, x) == 255)
                {
                    for (int index = y; index < y + iterationStep; index++)
                    {
                        image.at<uchar>(index, x) = 255;
                    }
                    botPoint = true;
                }

                if (rightPoint && botPoint)
                {
                    for (int indexX = x; indexX < x + iterationStep; indexX++)
                    {
                        for (int indexY = y; indexY < y + iterationStep; indexY++)
                        {
                            image.at<uchar>(indexY, indexX) = 255;
                        }
                    }
                }
            }
        }
    }
}
