This is a very basic Qr code detector created from scratch in C++ using OpenCV.
The code is not optimized and not very accurate. It's purpose was to make an
app that can place some text on a screen based on the Qr code position.