# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.16

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /snap/cmake/203/bin/cmake

# The command to remove a file.
RM = /snap/cmake/203/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/alex/Projects/QrDetector

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/alex/Projects/QrDetector/build

# Include any dependencies generated for this target.
include CMakeFiles/QrDetector.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/QrDetector.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/QrDetector.dir/flags.make

CMakeFiles/QrDetector.dir/main.cpp.o: CMakeFiles/QrDetector.dir/flags.make
CMakeFiles/QrDetector.dir/main.cpp.o: ../main.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/alex/Projects/QrDetector/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/QrDetector.dir/main.cpp.o"
	/usr/bin/g++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/QrDetector.dir/main.cpp.o -c /home/alex/Projects/QrDetector/main.cpp

CMakeFiles/QrDetector.dir/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/QrDetector.dir/main.cpp.i"
	/usr/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/alex/Projects/QrDetector/main.cpp > CMakeFiles/QrDetector.dir/main.cpp.i

CMakeFiles/QrDetector.dir/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/QrDetector.dir/main.cpp.s"
	/usr/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/alex/Projects/QrDetector/main.cpp -o CMakeFiles/QrDetector.dir/main.cpp.s

CMakeFiles/QrDetector.dir/sources/qrDetector.cpp.o: CMakeFiles/QrDetector.dir/flags.make
CMakeFiles/QrDetector.dir/sources/qrDetector.cpp.o: ../sources/qrDetector.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/alex/Projects/QrDetector/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building CXX object CMakeFiles/QrDetector.dir/sources/qrDetector.cpp.o"
	/usr/bin/g++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/QrDetector.dir/sources/qrDetector.cpp.o -c /home/alex/Projects/QrDetector/sources/qrDetector.cpp

CMakeFiles/QrDetector.dir/sources/qrDetector.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/QrDetector.dir/sources/qrDetector.cpp.i"
	/usr/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/alex/Projects/QrDetector/sources/qrDetector.cpp > CMakeFiles/QrDetector.dir/sources/qrDetector.cpp.i

CMakeFiles/QrDetector.dir/sources/qrDetector.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/QrDetector.dir/sources/qrDetector.cpp.s"
	/usr/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/alex/Projects/QrDetector/sources/qrDetector.cpp -o CMakeFiles/QrDetector.dir/sources/qrDetector.cpp.s

# Object files for target QrDetector
QrDetector_OBJECTS = \
"CMakeFiles/QrDetector.dir/main.cpp.o" \
"CMakeFiles/QrDetector.dir/sources/qrDetector.cpp.o"

# External object files for target QrDetector
QrDetector_EXTERNAL_OBJECTS =

QrDetector: CMakeFiles/QrDetector.dir/main.cpp.o
QrDetector: CMakeFiles/QrDetector.dir/sources/qrDetector.cpp.o
QrDetector: CMakeFiles/QrDetector.dir/build.make
QrDetector: /usr/local/lib/libopencv_dnn.so.4.1.2
QrDetector: /usr/local/lib/libopencv_gapi.so.4.1.2
QrDetector: /usr/local/lib/libopencv_highgui.so.4.1.2
QrDetector: /usr/local/lib/libopencv_ml.so.4.1.2
QrDetector: /usr/local/lib/libopencv_objdetect.so.4.1.2
QrDetector: /usr/local/lib/libopencv_photo.so.4.1.2
QrDetector: /usr/local/lib/libopencv_stitching.so.4.1.2
QrDetector: /usr/local/lib/libopencv_video.so.4.1.2
QrDetector: /usr/local/lib/libopencv_videoio.so.4.1.2
QrDetector: /usr/local/lib/libopencv_imgcodecs.so.4.1.2
QrDetector: /usr/local/lib/libopencv_calib3d.so.4.1.2
QrDetector: /usr/local/lib/libopencv_features2d.so.4.1.2
QrDetector: /usr/local/lib/libopencv_flann.so.4.1.2
QrDetector: /usr/local/lib/libopencv_imgproc.so.4.1.2
QrDetector: /usr/local/lib/libopencv_core.so.4.1.2
QrDetector: CMakeFiles/QrDetector.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/alex/Projects/QrDetector/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Linking CXX executable QrDetector"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/QrDetector.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/QrDetector.dir/build: QrDetector

.PHONY : CMakeFiles/QrDetector.dir/build

CMakeFiles/QrDetector.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/QrDetector.dir/cmake_clean.cmake
.PHONY : CMakeFiles/QrDetector.dir/clean

CMakeFiles/QrDetector.dir/depend:
	cd /home/alex/Projects/QrDetector/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/alex/Projects/QrDetector /home/alex/Projects/QrDetector /home/alex/Projects/QrDetector/build /home/alex/Projects/QrDetector/build /home/alex/Projects/QrDetector/build/CMakeFiles/QrDetector.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/QrDetector.dir/depend

